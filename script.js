$(document).ready(function () {

    var xhttp = new XMLHttpRequest(); 
    xhttp.open("GET", "ob.json", true);
    xhttp.send();
     xhttp.onreadystatechange = function () { 
      
         console.log(this.responseText);
         let obj = JSON.stringify(this.responseText);
            $("#first").text("with http " + obj);
        }

    $.ajax({
        url: "ob.json",
        success: function(data, textStatus, jqXHR){
                $("#result").text(
                    `With ajax : ${data.name} is a ${data.job} and she has ${data.skills[0].skill} and ${data.skills[1].skill} skills`
                );
                console.log(data);
        }
    })
    
});